
SHELL = /bin/sh
CXX = g++
CXXFLAGS = -std=c++11 -g -ggdb -Wall -Isrc/ -I/usr/include/SDL2
LDFLAGS = -lSDL2 -lGLEW -lGL
OBJDIR = build
SRCDIR = src
BINDIR = bin
BIN = $(BINDIR)/shader_testing


SOURCES = $(wildcard $(SRCDIR)/*.cpp)
OBJECTS = $(patsubst $(SRCDIR)/%.cpp, $(OBJDIR)/%.o, $(SOURCES))
NDBG_SOURCES = $(wildcard $(SRCDIR)/*.cc)
NDBG_OBJS = $(patsubst $(SRCDIR)/%.cc, $(OBJDIR)/%.o, $(NDBG_SOURCES))


all: $(BIN)
.PHONY: all

-include $(OBJDIR)/*.d

$(OBJECTS): $(OBJDIR)/%.o : $(SRCDIR)/%.cpp
	$(CXX) $(CXXFLAGS) -c -MMD $< -o $@

$(NDBG_OBJS): $(OBJDIR)/%.o : $(SRCDIR)/%.cc
	$(CXX) -std=c++11 -Wall -Isrc/ -c $< -o $@
	strip -d $@

$(BIN): $(OBJECTS) $(NDBG_OBJS)
	mkdir -p $(BINDIR)
	$(CXX) -o $@ $(LDFLAGS) $^

clean:
	rm -rf $(BIN)
	rm -rf build/*
.PHONY: clean

