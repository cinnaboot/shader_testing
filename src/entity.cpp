
#include <glm/gtc/matrix_transform.hpp>

#include "dumbLog.h"
#include "entity.h"


bool
initEntity(Entity* e,
	GLContext* gl_ctx,
	MemoryArena* arena,
	Model* mdl,
	u32 num_attrib_mappings,
	GLBufferToAttribMapping* attrib_mappings,
	const char* name)
{
	e->num_meshes = mdl->num_meshes;
	e->meshes = ARENA_ALLOC(arena, GLMesh, e->num_meshes);
	e->model_xform = ARENA_ALLOC(arena, glm::mat4, 1);
	*e->model_xform = glm::mat4(1.f);
	e->name = arenaCopyCStr(arena, name);

	if (mdl->diffuse_texture) {
		e->diffuse_texture = getGLTexture(gl_ctx, mdl->diffuse_texture);

		if (!e->diffuse_texture)
			return false;
	}

	for (u32 i = 0; i< e->num_meshes; i++) {
		GLMesh* glm = &e->meshes[i];
		*glm = loadGLMesh(arena,
						mdl->meshes[i],
						GL_TRIANGLES,
						e->diffuse_texture,
						num_attrib_mappings,
						attrib_mappings);

		if (glm->vao_id == 0) {
			LOGF(Error, "error initializing entity\n");
			return false;
		}
	}

	return true;
}

void
setEntityPosition(Entity* e, glm::vec3 pos)
{
	(*e->model_xform)[3][0] = pos.x;
	(*e->model_xform)[3][1] = pos.y;
	(*e->model_xform)[3][2] = pos.z;
}

void
rotateEntity(Entity* e, glm::vec3 axis, float radians)
{
	*e->model_xform = glm::rotate(*e->model_xform, radians, axis);
}

void
scaleEntity(Entity* e, float scale)
{
	*e->model_xform =
		glm::scale(*e->model_xform, glm::vec3(scale, scale, scale));
}

