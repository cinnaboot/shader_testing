
#pragma once


const char* DUMMY_VERTEX_SHADER = R"VS(
#version 330 core
layout (location = 0) in vec3 position;

uniform mat4 world_transform;
uniform mat4 MVP;


void main()
{
	gl_Position =  MVP * world_transform * vec4(position, 1);
}
)VS";

const char* DUMMY_FRAGMENT_SHADER = R"FS(
#version 330 core

out vec4 color;


void main()
{
	color = vec4(1, 0, 0, 1);
}
)FS";

