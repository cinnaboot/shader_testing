
#pragma once

#include "asset.h"
#include "shader.h"
#include "types.h"
#include "util.h"


struct Entity
{
	u32 num_meshes;
	GLMesh* meshes;
	GLTexture* diffuse_texture;	// NOTE: pointer into gl_ctx->textures array
	glm::mat4* model_xform;
	char* name;
};


bool initEntity(Entity* e,
	GLContext* gl_ctx,
	MemoryArena* arena,
	Model* mdl,
	u32 num_attrib_mappings,
	GLBufferToAttribMapping* attrib_mappings,
	const char* name = "");

void setEntityPosition(Entity* e, glm::vec3 pos);

void rotateEntity(Entity* e, glm::vec3 axis, float radians);

void scaleEntity(Entity* e, float scale);
