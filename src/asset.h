
#pragma once

#include <GL/glew.h>
#include <glm/glm.hpp>

#include "types.h"
#include "util.h"


// NOTE: wrapper for stb_image
struct Texture
{
	i32 w;
	i32 h;
	i32 bits_per_channel;
	i32 num_channels;
	uint data_len;
	u8* pixels;
	u64 filepath_hash;
	char file_path[256];
};

// NOTE: wrapper for tinygltf https://github.com/syoyo/tinygltf
// 	https://github.com/KhronosGroup/glTF
struct Mesh
{
	u32 num_vertices;
	u32 num_indices;
	glm::vec3* vertices;
	glm::vec3* normals;
	glm::vec2* uvs;
	glm::vec3* colors;
	u16* indices; // NOTE: u16 to match tinygltf library output
	glm::mat4* xform;
};

#define MAX_PATH_SIZE 256
struct Model
{
	char* filepath;
	u64 filepath_hash;
	uint num_meshes;
	Mesh* meshes;
	Texture* diffuse_texture;
};

struct Assets
{
	MemoryArena* arena;
	u32 num_models;
	u32 max_models;
	Model* models;

	u32 num_textures;
	u32 max_textures;
	Texture* textures;
};


Model* getModelByPath(Assets* assets, const char* filepath);

Texture* getTextureByPath(Assets* assets, const char* filepath);

