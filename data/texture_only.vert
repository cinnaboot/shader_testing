#version 330 core

layout (location = 0) in vec3 position;
layout (location = 1) in vec2 uv;

out vec2 frag_uv;

layout (std140) uniform matrices
{
	mat4 view_xform;
	mat4 proj_xform;
	mat4 normal_xform;
};

uniform mat4 node_xform;


void main()
{
	frag_uv = uv;
	gl_Position = proj_xform * view_xform * node_xform * vec4(position, 1);
}
