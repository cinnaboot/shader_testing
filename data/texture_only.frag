#version 330 core

in vec2 frag_uv;

out vec4 color;

layout (std140) uniform matrices
{
	mat4 view_xform;
	mat4 proj_xform;
	mat4 normal_xform;
};

uniform mat4 node_xform;
uniform sampler2D sampler;


void main()
{
	color = texture(sampler, frag_uv.st);
}
