#version 330 core

layout (location = 0) in vec3 position;
layout (location = 1) in vec3 normal;

out vec3 frag_normal;

layout (std140) uniform matrices
{
	mat4 view_xform;
	mat4 proj_xform;
	mat4 normal_xform;
};

uniform mat4 node_xform;


void main()
{
	// TODO: probably better to do this once in a separate uniform than once
	// 	for every vertex
	mat4 xform = node_xform;
	xform[3] = vec4(0, 0, 0, 1); // NOTE: undo translation
	frag_normal = vec4(xform * vec4(normal, 1)).xyz;
	gl_Position = proj_xform * view_xform * node_xform * vec4(position, 1);
}
